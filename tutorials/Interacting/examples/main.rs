use sqlx::postgres::PgPoolOptions;

#[async_std::main]
async fn main() -> Result<(), sqlx::Error> {
    let pool = PgPoolOptions::new()
        .max_connections(7)
        // .connect("mysql://root:@localhost/classroom_diesel")
        .connect("postgresql://")
        .await?;

//     sqlx::query("UPDATE students SET firstname=?, lastname=? WHERE id=?")
//         .bind("Richard")
//         .bind("Roe")
//         .bind(1)
//         .execute(&pool)
//         .await?;

//     sqlx::query("DELETE FROM students WHERE id=?")
//         .bind(1)
//         .execute(&pool)
//         .await?;

    Ok(())
}

// https://blog.logrocket.com/interacting-databases-rust-diesel-vs-sqlx/#getting-started-sqlx
